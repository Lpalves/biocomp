import numpy as np
def loadFile(): 
    file = open("leukemia_big.csv")
    sequence = '' 
    fileArray = file.readlines()
    for x in range(len(fileArray)):
        fileArray[x] = fileArray[x][:-2]
        fileArray[x]=fileArray[x].split(',')
    return fileArray

def clusterPoints(X, mu):
    clusters  = {}
    for x in X:
        centers = min([(i[0], np.linalg.norm(x-mu[i[0]])) \
                    for i in enumerate(mu)], key=lambda t:t[1])[0]
        try:
            clusters[centers].append(x)
        except KeyError:
            clusters[centers] = [x]
    return clusters

def updateCenters(mu, clusters): #update new centes
    newCenter = []
    keys = sorted(clusters.keys())
    for k in keys:
        newCenter.append(np.mean(clusters[k], axis = 0))
    return newCenter

def converged(center, oldCenter): # boolean, returns if converged
  return (set([tuple(a) for a in center]) == set([tuple(a) for a in oldCenter]))

def findCenters(X, K):
  # Initialize to K random centers
  oldCenter = random.sample(X, K)
  actualCenter = random.sample(X, K)
  while not converged(actualCenter, oldCenter):
    oldCenter = actualCenter
    # assign all points in X to clusters
    clusters = clusterPoints(X, actualCenter)
    # update centers
    actualCenter = updateCenters(oldCenter, clusters)
  return(actualCenter, clusters)

import random

def randomCoordinates(N):
  X = np.array([(random.uniform(-1, 1), random.uniform(-1, 1)) for i in range(N)])
  return X

#tests
x=loadFile()
original = x[0]
del x[0]
samplesArray=[]
for x in range(len(sampless[0])):
  samplesArray.append([])
  for y in range(len(sampless)):
    samplesArray[x].append(sampless[y][x])

samplesArray = np.asarray(samplesArray)
result = findCenters(samplesArray,2)
controids=result[0]
clusters=result[1]

#cria um dicionario com os andices de cada amostra
samples={}
for x in range(len(original)):
  if original[x]=='AML':
    samples[x]= 'AML'
  else:
    samples[x]='ALL'

#contar AMLs e ALLs em cada cluster
count = [[0,0],[0,0]]
for x in range(len(clusters)):
  for y in range(len(clusters[x])):
    idSamples = clusters[x][y][0]
    if samples[idSamples]== 'ALL':
      count[x][0]=count[x][0]+1
    if samples[idSamples]== 'AML':
      count[x][1]=count[x][1]+1

amostrasCluster = {}