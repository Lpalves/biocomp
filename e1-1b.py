Dic={'A':'T','T':'A','G':'C','C':'G',}
def isPalindrome(sequence):
    fim=len(sequence)-1
    tamanho = fim+1
    if sequence[0] == Dic[sequence[fim]] and tamanho == 2:     #compara o primeiro caracter com o ultimo
        return True
    elif sequence[0] == Dic[sequence[fim]]:
        return isPalindrome(sequence[1:len(sequence)-1])
    else:
        return False
def loadSequence(): 
    file = open("sequence.fasta")
    sequence = '' 
    fileArray = file.readlines()
    del fileArray[0]
    for x in fileArray:
        
        sequence += x[:-2]
    return sequence
def contPalindrome(sequence):
    cont=0
    for x in range(len(sequence)-18):
        subSequence=sequence[x:x+18]
        if subSequence[0] !='N' and subSequence[17] != 'N':
            if isPalindrome(subSequence):
                cont+=1
    return cont