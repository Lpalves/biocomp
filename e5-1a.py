#Programa incompleto pois não consegui implementar todo
#Da parte concluída, Eu segmentei o codigo por funções para cada etapa do algoritmo. possibilitando facil recursividade e legibilidade
#Falta a função que monta a arvore numa estrutura criada baseado no nó recebido

import numpy
matrix = [[0,13,21,22],[13,0,12,13],[21,12,0,13],[22,13,13,0]]
def makeDStar(matrix):
  totalDistance = []
  lineRange = range(len(matrix))
  columnRange = range(len(matrix[0]))
  D = [[0 for x in lineRange] for y in columnRange]
  for i in lineRange:
    totalDistance.append(sum(matrix[i]))
  for i in lineRange:        #i=linha
    print i
    for j in columnRange:    #j=coluna
      if i == j:
        D[i][j] = 0
      else:
        D[i][j]=(len(lineRange)-2)*matrix[i][j]-totalDistance[i]-totalDistance[j]
  return D, totalDistance

def findMinimumElement(matrix):
  minorElement = 0
  matrixRange = len(matrix)
  for i in matrixRange:          #i=linha
    for j in matrixRange: 
      if i > j and matrix[i][j] >= minorElement:
        minorElement = matrix[i][j]
  return minorElement

def reduceMatrix(matrix, i ,j):
  totalDistance = []
  matrixRange = range(len(matrix)-1)
  newMatrix = [[0 for x in matrixRange] for y in matrixRange]
  for x in matrixRange:           #i=linha
    for y in matrixRange: 
      if x == i:
        newMatrix[i][j] = matrix[i][j]

def delta(DStarMatrix,i,j):
  delta = (sum(DStarMatrix[i])-sum(DStarMatrix[j]))/(len(DStarMatrix)-2)
  return delta

makeDStar(matrix)