import numpy as np
def loadSequence(): 
    file = open("leukemia_big.csv")
    sequence = '' 
    fileArray = file.readlines()
    for x in range(len(fileArray)):
        fileArray[x] = fileArray[x][:-2]
        fileArray[x]=fileArray[x].split(',')
    return fileArray

def cluster_points(X, mu):
    clusters  = {}
    for x in X:
        bestmukey = min([(i[0], np.linalg.norm(x-mu[i[0]])) \
                    for i in enumerate(mu)], key=lambda t:t[1])[0]
        try:
            clusters[bestmukey].append(x)
        except KeyError:
            clusters[bestmukey] = [x]
    return clusters

def reevaluate_centers(mu, clusters):
    newmu = []
    keys = sorted(clusters.keys())
    for k in keys:
        newmu.append(np.mean(clusters[k], axis = 0))
    return newmu

def has_converged(mu, oldmu):
  return (set([tuple(a) for a in mu]) == set([tuple(a) for a in oldmu]))

def find_centers(X, K):
  # Initialize to K random centers
  oldmu = random.sample(X, K)
  mu = random.sample(X, K)
  while not has_converged(mu, oldmu):
    oldmu = mu
    # Assign all points in X to clusters
    clusters = cluster_points(X, mu)
    # Reevaluate centers
    mu = reevaluate_centers(oldmu, clusters)
  return(mu, clusters)

import random

def init_board(N):
  X = np.array([(random.uniform(-1, 1), random.uniform(-1, 1)) for i in range(N)])
  return X

def init_board_gauss(N, k):
  n = float(N)/k
  X = []
  for i in range(k):
    c = (random.uniform(-1, 1), random.uniform(-1, 1))
    s = random.uniform(0.05,0.5)
    x = []
    while len(x) < n:
      a, b = np.array([np.random.normal(c[0], s), np.random.normal(c[1], s)])
      # Continue drawing points from the distribution in the range [-1,1]
      if abs(a) < 1 and abs(b) < 1:
        x.append([a,b])
    X.extend(x)
  X = np.array(X)[:N]
  return X

x=loadSequence()
original = x[0]
del x[0]
cord=[]
for a in range(72):
  for b in range(len(x)):
    cord.append([float(a),float(x[b][a])])

cord = np.asarray(cord)
result = find_centers(cord,2)
controides=result[0]
clusters=result[1]

#cria um dicionario com os andices de cada amostra
amostras={}
for x in range(len(original)):
  if original[x]=='AML':
    amostras[x]= 'AML'
  else:
    amostras[x]='ALL'

#contar AMLs e ALLs em cada cluster
contagem = [[0,0],[0,0]]
for x in range(len(clusters)):
  for y in range(len(clusters[x])):
    idAmostra = clusters[x][y][0]
    if amostras[idAmostra]== 'ALL':
      contagem[x][0]=contagem[x][0]+1
    if amostras[idAmostra]== 'AML':
      contagem[x][1]=contagem[x][1]+1

amostrasCluster = {}