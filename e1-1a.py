def loadSequence(): 
    file = open("sequence.fasta")
    sequence = '' 
    fileArray = file.readlines()
    del fileArray[0]
    for x in fileArray:
        sequence += x[:-2]
    return sequence
#questão A,
def findSubsequence(subsequence, sequence): #retorna o indice da localização da subsequencia
    aux = ['C','G','T','A']
    for x in range(len(subsequence)):           #monta todas variações de mutações possiveis para cada indica
        temp = subsequence[x]                   #backup do nucletideo a ser mutado para o teste das mutações   
        for y in range(len(aux)):               #"muta" o nucleotídeo no indice a ser alterado
            subsequence=list(subsequence)
            subsequence[x]=aux[y]               
            subsequence=''.join(subsequence)    
            indice = sequence.find(subsequence) #procura s subsequencia mutada na sequencia
            if indice !=-1:
                return indice
        subsequence=list(subsequence)
        subsequence[x]=temp

sequence = loadSequence()
findSubsequence("GATCTTCGTGGCCAC",sequence)
