import numpy
A="AGC"
B="AAAC"
matchScore = 1
gapScore = -2
mismatchPenalty = -1
gapPenalty = -2
def align(matrix,stringA,stringB):
    alignedStringA = ''     
    alignedStringB = ''
    score = 0
    identity=0                 
    x, y = len(matrix)-2,len(matrix[0])-2
    i, j = len(matrix)-1,len(matrix[0])-1
    #print x,y,i,j
    while i>0 and j>0:
        current = matrix[i][j]
        up = matrix[i-1][j]
        left = matrix[i][j-1]
        diagonal = matrix[i-1][j-1]
        if stringA[x]==stringB[y]:          #MATCH (move diagonal)
            #print "entro IF 1"
            score += matchScore             #atualiza score
            identity += 1                   #conta identidade
            alignedStringA += stringA[x]     
            alignedStringB += stringB[y]
            x,y = x-1, y-1                 #atualiza índices
            i,j = i-1, j-1     
        elif up > left and up > diagonal: #SE GAP stringA (move up)
            #print "entro IF 2"
            score =+ gapPenalty
            alignedStringB += '-'           #insere o gap no subsequencia1
            x -= 1                #atualiza índices
            i -= 1     
            alignedStringA += stringA[y]    
        elif left > diagonal:              #SE GAP stringB (move left)
            #print "entro IF 3"
            score += gapPenalty
            alignedStringA += '-'     
            j,y = j-1, y-1                 #atualiza índice      
            alignedStringB += stringB[y]
        else:                               #MISMATCH
            #print "entro IF 4"
            score += mismatchPenalty
            alignedStringA += stringA[x]
            alignedStringB += stringB[y]
            x,y = x-1, y-1               #atualiza índices
            i,j = i-1, j-1
    return alignedStringA, alignedStringB, identity

def createMatrix(string_A,string_B):
    j=len(string_A)+1
    i=len(string_B)+1
    M=numpy.zeros((j,i))
    for x in range(len(M)):
        M[x][0]=0
    for x in range(len(M[0])):
        M[0][x]=0
    for x in range(len(string_A)):
        for y in range(len(string_B)):    
            if string_A[x] == string_B[y]:
                M[x+1][y+1] = max(M[x][y] + matchScore , M[x+1][y] + gapPenalty , M[x][y+1] + gapPenalty)
            else:
                M[x+1][y+1]= max(M[x][y] + mismatchPenalty , M[x+1][y]+ gapPenalty , M[x][y+1]+ gapPenalty )
    return M


def findHigherScore(matrix_M): #return strings index on the higher score
    i, j = len(matrix_M),len(matrix_M[0])
    higher=0
    for x in range(i):
        for y in range(j):
            if higher <= matrix_M[x][y]:
                higher = matrix_M[x][y]
                I=x
                J=y
    return I-1,J-1 #,matrix_M[I][J]

def alignSequences(string_sequenceA,string_sequenceB):
    M = createMatrix(string_sequenceA,string_sequenceB)
    aligned1,aligned2,identity = align(M, string_sequenceA, string_sequenceB)
    return aligned1[::-1],aligned2[::-1],identity

def water(seqA,seqB):
    M = createMatrix(seqA,seqB)
    i,j = findHigherScore(M)
    print i,j
    aligned1,aligned2, identity = alignSequences(seqA[:i],seqB[:j]) 
    print "Sequencia alinhada 1: \n"+aligned1
    print "\n Sequencia alinhada 2: \n"+aligned2
    newM = createMatrix(seqA[:i],seqB[:j])
    i,j = len(newM)-1,len(newM[0])-1
    strs = ["" for x in range(i+1)]
    for x in range(len(newM)):
        for y in range(len(newM[0])):
            strs[x] += str(int(newM[x][y]))
    print M

    # for x in strs:
    #     print x


water(A,B)